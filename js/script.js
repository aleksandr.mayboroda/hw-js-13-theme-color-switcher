
// //variant # 1
let theme = localStorage.getItem('themeBtn') || 1,
    btnSwitcher = document.querySelector('[data-action="theme-switcher"]')
    
btnSwitcher.addEventListener('click',(event) => {
    theme = localStorage.getItem('themeBtn')
    theme = (theme == 1) ? 2 : 1
    localStorage.setItem('themeBtn',theme)
    toggleTheme()
})
toggleTheme()
function toggleTheme() {
    theme = localStorage.getItem('themeBtn')
    if(theme == 1)
    {
        document.body.classList.remove('dark-theme')
        btnSwitcher.classList.remove('btn-green')
        btnSwitcher.classList.add('btn-blue')
        btnSwitcher.innerText = 'Switch to Dark'
    }
    else
    {
        document.body.classList.add('dark-theme')
        btnSwitcher.classList.remove('btn-blue')
        btnSwitcher.classList.add('btn-green')
        btnSwitcher.innerText = 'Switch to Light'
    }
}


// //variant # 2
// let themeSwitcher = document.querySelector('[name="theme_toggle"]')

// themeSwitcher.value = localStorage.getItem('theme') || 1
// toggleTheme(themeSwitcher)

// themeSwitcher.addEventListener('change', (event) => {
//     let themeField = event.target
//     localStorage.setItem('theme',themeField.value)
//     toggleTheme(themeField)
// })

// function toggleTheme(select) {
//     let theme = select.value
//     if(theme == 1)
//     {
//         document.body.classList.remove('dark-theme')
//     }
//     else
//     {
//         document.body.classList.add('dark-theme')
//     }
// }